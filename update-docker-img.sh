#!/usr/bin/env bash

set -e
set -u

DOCKER_IMAGE_NAME='docker-registry.tools.pe.springer-sbm.com/springersbm/ml-materials'
DEPRECATED_DOCKER_IMAGE_NAME='patforna/ml-configured'
DOCKER_CONTAINER_NAME='marklogic'

run() {
  echo "Starting $DOCKER_IMAGE_NAME container ..."
  docker run -d --name $DOCKER_CONTAINER_NAME -p 7654:7654 -p 7655:7655 -p 7656:7656 -p 7657:7657 -p 8000:8000 -p 8001:8001 -p 8002:8002 -p 8400:8400 -p 8404:8404 -p 8501:8501 -p 9999:9999 "$DOCKER_IMAGE_NAME"
}

stopAndRm() {
  for id in $(docker ps -aq); do
    imageName=$(docker inspect -f '{{ .Config.Image }}' "$id")
    running=$(docker inspect -f '{{ .State.Running }}' "$id")
    if [ "$imageName" = "$DOCKER_IMAGE_NAME" ] || [ "$imageName" = "$DEPRECATED_DOCKER_IMAGE_NAME" ]; then
      if [ "$running" = true ]; then
        echo "Stopping $DOCKER_IMAGE_NAME container $id ..."
        docker stop "$id"
      fi
      echo "Removing $DOCKER_IMAGE_NAME container $id ..."
      docker rm "$id"
    fi
  done
}

stopAndRun() {
  stopAndRm
  run
}

pull() {
  echo "Checking for updates to Docker image '$DOCKER_IMAGE_NAME'..."
  set +e
  docker pull "$DOCKER_IMAGE_NAME"
  if [ "$?" = "1" ]; then
      echo "Unable to do docker pull $DOCKER_IMAGE_NAME "
      echo "Have you run 'docker login' on this machine?"
      echo "Check ~/.dockercfg"
      echo "See Docker registry login details here: https://confluence.springer-sbm.com/display/CSP/Docker+Hub+registry:+SpringerSBM+group"
      exit 1
  fi
  set -e
}

getLocalRepoImageId() {
  docker images -q "$DOCKER_IMAGE_NAME"
}

getRunningContainerId() {
  docker ps | grep "$DOCKER_IMAGE_NAME" | cut -f1 -d' ' 
}

getImageIdForContainer() {
  if [ "$1" != "" ]; then
    # We only want the first 12 characters because that is what we get from `docker ps`
    docker inspect -f '{{ .Image }}' "$1" | cut -c1-12
  else 
    echo "Error: no container id supplied"
    exit 1
  fi
}

dieIfNoDocker() {
  command -v docker >/dev/null 2>&1 || { echo >&2 "Can't find Docker."; exit 1; }
}

isRunning() {
  runningContainerId=`getRunningContainerId`
  if [ -z "$runningContainerId" ]; then
    return 1
  else
    return 0
  fi
}

dieIfNoDocker
pull
stopAndRun
if isRunning; then
    echo "Docker container '$DOCKER_CONTAINER_NAME' is running."
    exit 0
else
    echo "[ERROR] Docker container '$DOCKER_CONTAINER_NAME' is not running after restart. Check container logs for errors by running command:
$ docker logs $DOCKER_CONTAINER_NAME"
    exit 1
fi
